package com.example.app2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.app2.databinding.FragmentMinusBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

class Minusfragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentMinusBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMinusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.MinusMarkEqualTo.setOnClickListener {
            var num1 = binding.NumberNumber1.text.toString().toInt()
            var num2 = binding.NumberNumber2.text.toString().toInt()
            var answerminus = num1 - num2
            val action =
                MinusfragmentDirections.actionMinusfragmentToAnswerMinusfragment(answerminus = answerminus.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}