package com.example.app2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.app2.databinding.FragmentAnswerminusBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

class AnswerMinusfragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentAnswerminusBinding? = null
    private  val binding get() = _binding!!
    private lateinit var answerminus: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        answerminus = arguments?.getString(ANSWERMINUS).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAnswerminusBinding.inflate(inflater,container, false)
        binding.textViewAnswer.text = answerminus
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonRestartMinus.setOnClickListener {
            val action = AnswerMinusfragmentDirections.actionAnswerMinusfragmentToHomefragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {
        // TODO: Rename and change types and number of parameters
        const val ANSWERMINUS = "answerminus"
    }
}