package com.example.app2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.app2.databinding.FragmentAnswerplusBinding


// TODO: Rename parameter arguments, choose names that match

class AnswerPlusfragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentAnswerplusBinding? = null
    private val binding get() = _binding!!
    private lateinit var answer: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        answer = arguments?.getString(ANSWER).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAnswerplusBinding.inflate(inflater,container, false)
        binding.textViewAnswer.text = answer
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonRestartPlus.setOnClickListener {
            val action = AnswerPlusfragmentDirections.actionAnswerFragmentToHomefragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {
        // TODO: Rename and change types and number of parameters
       const val ANSWER = "answer"
    }
}